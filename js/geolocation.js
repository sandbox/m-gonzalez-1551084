(function ($) {
	Drupal.behaviors.geolocation_module = {
		attach: function(context) {


			  initialize();
			  /* attach a submit handler to the form */
			  $("#geolocation-wrapper #geolocation-submit").click(function(event) { 
				  /* stop form from submitting normally */
				  event.preventDefault();
				  
				  geolocalizame();
			  });
		
			  $("#geolocation-wrapper #geolocation-manual-submit").click(function(event) { 
				  /* stop form from submitting normally */
				  event.preventDefault();
				  var address = $("#geolocation-wrapper #edit-compartir #edit-coor").val(); 
				  if(address=="") {
					  alert(Drupal.t('You must enter an address'));
				  }
				  else {
					  geolocalizame_street(address);
				  }
			  });
	  
		}//end attach
	};//end behaviors
	

/**
 * 
 */
function initialize() {
	
  var latitud=$("#geolocation-wrapper .latitud-map").attr('id');
  var longitud=$("#geolocation-wrapper .longitud-map").attr('id');

  var punto = new google.maps.LatLng(latitud, longitud); //ubicación
  
  var myOptions = {
    zoom: 18, //nivel de zoom para poder ver de cerca.
    center: punto,
    mapTypeId: google.maps.MapTypeId.ROADMAP //Tipo de mapa inicial, satélite para ver las pirámides
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  marker = new google.maps.Marker({
	    map: map, 
	    position: punto
  });
}

/**
 * 
 * @param pos
 */
function pedirPosicion(pos) {
  var centro = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
  map.setCenter(centro); //pedimos que centre el mapa..
  map.setMapTypeId(google.maps.MapTypeId.ROADMAP); //y lo volvemos un mapa callejero
  var datos = "latitud="+pos.coords.latitude+"&longitud="+pos.coords.longitude;
 
  marker = new google.maps.Marker({
	    map: map, 
	    position: centro
  });
  
  $.ajax({ 
	  data: datos, 
	  type: "POST", 
	  dataType: "json", 
	  url: "/geolocation/js"
  });

}
/**
 * 
 */
function geolocalizame() {
	navigator.geolocation.getCurrentPosition(pedirPosicion);
}

/**
 * 
 * @param string address
 *   direccion de google maps
 */
function geolocalizame_street(address) {
	var geocoder = new google.maps.Geocoder();
	
	geocoder.geocode( { 'address': address}, function(results, status) {
	  if (status == google.maps.GeocoderStatus.OK) {
		  //obtendo la latitud y longitud
		  var latitud=results[0].geometry.location.lat();
		  var longitud=results[0].geometry.location.lng();
		  
		  var centro = new google.maps.LatLng(latitud,longitud);
		  map.setCenter(centro); //pedimos que centre el mapa..
		  map.setMapTypeId(google.maps.MapTypeId.ROADMAP); //y lo volvemos un mapa callejero
		  var datos = "latitud="+latitud+"&longitud="+longitud;
		  
		  marker = new google.maps.Marker({
			    map: map, 
			    position: centro
	      });
		  $.ajax({ 
			  data: datos, 
			  type: "POST", 
			  dataType: "json", 
			  url: "/geolocation/js", 
		  });
	  }
	  else {
		  alert(Drupal.t('Unable to find the position'));
	  }
	});
	
}


})(jQuery);
