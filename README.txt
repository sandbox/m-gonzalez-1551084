Description
-----------
Este modulo a�ade una geoposici�n de usuarios en su Drupal Site
Dispondr� de la posibilidad de editar la posici�n de cada usuario desde la edici�n del perfil.
Se habilita un theme con un argumento que es el id de un usuario, con el cual, podremos visualizar la geoposici�n de un usuario

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire geolocation directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/geolocation

